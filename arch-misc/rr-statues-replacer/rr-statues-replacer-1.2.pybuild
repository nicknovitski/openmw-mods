# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "RR Mod Series - Morrowind Statues Replacer"
    DESC = "Pluginless replacer for different Morrowind statues"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43348"
    KEYWORDS = "openmw"
    LICENSE = "attribution"
    RDEPEND = "base/morrowind"
    TEXTURE_SIZES = "2048"
    NEXUS_SRC_URI = """
        https://nexusmods.com/morrowind/mods/43348?tab=files&file_id=1000015464
        -> RR_-_Morrowind_Statues_Replacer-43348-1-2-1565967418.rar
    """
    IUSE = "l10n_ru marble"
    # Included as a modder's resource
    # https://nexusmods.com/morrowind/mods/43348?tab=files&file_id=1000016263
    # -> Hermaeus_Mora-43348-1-0-1573667407.rar"
    INSTALL_DIRS = [
        InstallDir("00 - Azura Stone", REQUIRED_USE="!marble"),
        InstallDir("01 - Azura Marble", REQUIRED_USE="marble"),
        InstallDir("02 - Vivec Statues"),
        InstallDir(
            "03 - Optional - Vivec Statues Coda Fix English",
            PLUGINS=[File("RR_Vivec_Statues_Eng.esp")],
            REQUIRED_USE="!l10n_ru",
        ),
        InstallDir(
            "04 - Optional - Vivec Statues Coda Fix Russian",
            PLUGINS=[File("RR_Vivec_Statues_1C.ESP")],
            REQUIRED_USE="l10n_ru",
        ),
        # InstallDir("RR - Hermaeus Mora", S="Hermaeus_Mora-43348-1-0-1573667407"),
    ]
