Please credit the original authors first if the asset you want to use appears in this list (search using the corresponding Construction Set ID without suffix numbers); "edited" indicates that the meshes or textures of the original asset have been altered or compressed in TR.

The following asset list aims to include:
- by default, all separately released resources that were not directly contributed to TR by their authors but included based on their usage permissions, starting with the "Sacred East" release (June 2012),
- additionally, any assets from contributors past, present or future who request separate documentation here

This is retroactively documented in 2017 and therefore prone to oversights. Should anyone find they have been omitted from this list, please let us know.

author:    Momo77
---------------------------------
added Feb 2012, from:     Momos Crane Resource - http://www.nexusmods.com/morrowind/mods/41074/?
IDs:    T_Com_SetHarbor_CraneLarge_01 # to # 03
	T_Com_SetHarbor_CraneMiddle_01 # to # 02
	T_Com_SetHarbor_CranePlatfL_01
	T_Com_SetHarbor_CranePlatfS_01
	T_Com_SetHarbor_CranePlatfVL_01
	T_Com_SetHarbor_CraneSmall_01
added Feb 2012, from:     Momos Fishery Resource - http://www.nexusmods.com/morrowind/mods/41173/?
IDs:    T_Com_SetHarbor_DipNet_01 # to # 02
	T_Com_SetHarbor_FishBeheaded_01 # to # 02
	T_Com_SetHarbor_FishHanging_01 # to # 02
	T_Com_SetHarbor_Fishnet_01 # to # 02
	T_Com_SetHarbor_FishTrap_01
	T_Com_SetHarbor_Rope_01

author:    Slartibartfast
---------------------------------
added Aug 2012, from:     Smith Shed Resource - http://www.nexusmods.com/morrowind/mods/42183/?
IDs:    T_Imp_SetMw_X_SmithShed_01
	T_Imp_SetNord_X_SmithShed_01
	T_Nor_SetSkaal_SmithShed_01

author:    t'nilc
---------------------------------
added Aug 2013, from:     Hookah - http://mw.modhistory.com/download-1-11970
IDs:    T_Rga_HookahApp_01
	T_Rga_HookahFlavouredApp_01

author:    Midgetalien
---------------------------------
added Aug 2013, from:     Stick Fences -     http://www.nexusmods.com/morrowind/mods/42471/?
IDs:    T_Com_Set_FenceBriar_01 # to # 03
added Dec 2013, from:    Goblin Shaman - http://mw.modhistory.com/download-44-13318
IDs:    T_Mw_Cre_GobShm_01 (edited)

author:    dongle
---------------------------------
added Jul 2014, from:     Elizabethan Galleon - http://mw.modhistory.com/download-44-2980
IDs:    T_Imp_SetHarbor_GalleonNavy_01 (edited)
	T_Imp_SetHarbor_GalleonInLow_01 (edited)
	T_Imp_SetHarbor_GalleonInUp_01 (edited)

author:    Detritus2004
---------------------------------
added Dec 2016, from:     Khajiit Faces v2.0 - http://mw.modhistory.com/download-43-3301
IDs:    T_B_Kha_HeadMaleTR_02 # to # 09 (edited)
	T_B_Kha_HeadFemTR_01 # to # 09 (edited)
	T_B_Kha_HairMaleTR_01 # to # 02

author:    Silaria
---------------------------------
added Dec 2016, from:     Sils Argonian Heads and Hair v1.1 - http://mw.modhistory.com/download-80-3324
IDs:    T_B_Arg_HeadMaleTR_02 # to # 07 (edited)
	T_B_Arg_HeadFemTR_01 # to # 06 (edited)
	T_B_Arg_HairMaleTR_01 # to # 07 (edited)
	T_B_Arg_HairFemTR_01 # to # 04 (edited)

author:    R-Zero
---------------------------------
added Mar 2017, from:     TR Silt Strider fix - http://www.nexusmods.com/morrowind/mods/43297/?
IDs:    T_Mw_Fau_Slstrid_01

author:    Alaisiagae
---------------------------------
added Jul 2017, from:     Imperial Silver Armor Resource - http://mw.modhistory.com/download-56-6598
IDs:    T_Imp_Silver_Boots_01 (edited)
    T_Imp_Silver_BracerL_01 (edited)
    T_Imp_Silver_BracerR_01 (edited)
    T_Imp_Silver_Greaves_01 (edited)
    T_Imp_Silver_PauldronL_01 (edited)
    T_Imp_Silver_PauldronR_01 (edited)

author:    YarYulme
---------------------------------
added Jan 2018, from:     Nif Resources - https://www.nexusmods.com/morrowind/mods/43064/?
IDs:    T_Imp_Uni_SwiftcutSaber

author:    Lochnarus
---------------------------------
added Jan 2018, from:     Elite Dark Brotherhood Helm v1.3 - http://mw.modhistory.com/download-80-7683
IDs:    T_De_UNI_PreyseekerHelm (edited)

author:    Antares
---------------------------------
added Jan 2018, from:     Undead: Arise From Death - http://mw.modhistory.com/download-55-15310
IDs:    T_Mw_Und_Mum_02 (edited)

author:    Lougian
---------------------------------
added Jan 2018, from:     Caverns Overhaul - https://www.nexusmods.com/morrowind/mods/42249/?
IDs:    T_Glb_Light_CaveRay01 # to # 12 (updated)

Tamriel Rebuilt is a not-for-profit development team made up entirely of fans. We are not in any way, shape or form associated or affiliated with Bethesda Softworks, Zenimax, or any other entity involved in Morrowind's original development. The Elder Scrolls is a registered trademark of Bethesda Softworks. All additional content produced outside Bethesda Softworks and the TES Construction Set remains the intellectual property of its creators.

All rights to original game assets belong to Bethesda Softworks. Additional assets created for Tamriel Rebuilt are the property of their creators first, and the organization second. The following resources should only be used in mods with a dependency on Tamriel_data: Molecrab, Riverstrider, Telvanni Cephalopod. All other assets are free to use and redistribute in other modifications provided that their original author (where applicable) or Tamriel Rebuilt are duly credited.
The content of this archive is © Tamriel Rebuilt Community 2001-2018.
http://www.tamriel-rebuilt.org
